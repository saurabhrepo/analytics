package org.sood.javabrains.messengerProject.messages.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.sood.javabrains.messengerProject.messages.database.DatabaseClass;
import org.sood.javabrains.messengerProject.messages.model.Message;

public class MessageService {
	
	public MessageService(){
		messages.put((long) 1, new Message(1, "Hi", "Hi1"));
		messages.put((long) 2, new Message(2, "Helo", "Hi2"));
	}
	
	private Map<Long, Message> messages = DatabaseClass.getMessages();
	
	public List<Message> getAllMessages(){
			return new ArrayList<Message>(messages.values());
		
		
	}
	
	public Message getMessage (long id){
		return messages.get(id);
	}
	

	public Message addMessage(Message message){
		message.setId(messages.size()+1);
		messages.put(message.getId(), message);
		return message;
	}
	
	public Message updateMessage (Message message){
		if (message.getId()<=0){
			return null;		
		}
		messages.put(message.getId(), message);
		return message;
		
	}
	
	public Message removeMessage(long id){
		return messages.remove(id);
	}
	
	

}
