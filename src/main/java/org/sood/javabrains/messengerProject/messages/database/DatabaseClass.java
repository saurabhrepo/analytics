package org.sood.javabrains.messengerProject.messages.database;

import java.util.HashMap;
import java.util.Map;

import org.sood.javabrains.messengerProject.messages.model.Message;
import org.sood.javabrains.messengerProject.messages.model.Profile;

public class DatabaseClass {
	
	private static Map<Long, Message> messageMap = new HashMap<>();
	private static Map<Long, Profile> profileMap = new HashMap<>();
	
	public static Map<Long, Message> getMessages(){
		return messageMap;
	}
	
	public static Map<Long, Profile> getProfiles(){
		return profileMap;
	}

}
