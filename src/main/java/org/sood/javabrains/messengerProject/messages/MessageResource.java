package org.sood.javabrains.messengerProject.messages;

import java.awt.MediaTracker;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.sood.javabrains.messengerProject.messages.model.Message;
import org.sood.javabrains.messengerProject.messages.services.MessageService;


@Path("/messages")
public class MessageResource {
	
	MessageService msgSvc = new  MessageService();
	
	@GET
	@Produces (MediaType.APPLICATION_JSON)
	public List<Message> hello(){
		return msgSvc.getAllMessages();
		
	}
	
	@GET
	@Path("/{messageId}")
	@Produces (MediaType.APPLICATION_JSON)
	public Message hello(@PathParam("messageId") long messageId ){
		return msgSvc.getMessage(messageId);
		
	}
	
	@POST
	@Produces (MediaType.APPLICATION_JSON)
	@Consumes (MediaType.APPLICATION_JSON)
	public Message addMessage(Message message ){
		return msgSvc.addMessage(message);
		
	}
}
